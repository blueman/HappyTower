// ==UserScript==
// @name        happytower doors
// @namespace   happytower
// @downloadURL	https://gitlab.com/blueman/HappyTower/raw/master/doors.js
// @require     http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js
// @include     http://happytower.pl/doors
// @include     http://happytower.pl/doors?*
// @version     5
// @grant       none
// ==/UserScript==

var stopWhenYouFinishTheMaze = true;

if ($('a.btng').length == 1 || $('.door').length == 0) {
    if ($('span.white').length > 0 && $('span.white').html() == 'Nagroda:' && stopWhenYouFinishTheMaze) {
        // you win the door maze
    } else {
        setTimeout(function() {
            window.location.href = 'http://happytower.pl/doors';
        }, 2000);
    }
}

if (
    $('img.door').length >= 3
    && $('span.small:not(.minor) span').length == 1
    && $('span.small:not(.minor) span').text() > 1000
) {
    $('.door').eq(Math.floor(Math.random()*3)).click();
}