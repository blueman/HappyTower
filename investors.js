// ==UserScript==
// @name        happytower investors
// @namespace   happytower
// @downloadURL	https://gitlab.com/blueman/HappyTower/raw/master/investors.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js
// @include     http://happytower.pl/boss*
// @version     6
// @grant       none
// ==/UserScript==

var bossLinkPattern = getBossLinkPattern();

if (bossLinkPattern !== false) {
    setTimeout(function() {
        var link = $('a[href*="'+bossLinkPattern+'"]:first');
        if (link.attr('href').search('logoutLink') == -1) {
            link[0].click();
        }
    }, 2200);
}

function getBossLinkPattern()
{
    if ($('a[href*="../../boss"]').length > 0) {
        return '../../boss';
    } else if ($('a[href*="../boss"]').length > 0) {
        return '../boss';
    } else if ($('a[href*="boss"]').length > 0) {
        return 'boss';
    }
    return false;
}